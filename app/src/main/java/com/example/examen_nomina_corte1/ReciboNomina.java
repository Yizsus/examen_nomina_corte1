package com.example.examen_nomina_corte1;

import java.io.Serializable;

public class ReciboNomina implements Serializable {

    private String Nombre;
    private int horasTrabNormal;
    private int horasTrabExtras;
    private int puesto;
    private float impuesto;
    float subtotal = 0;
    float total=0;

    public ReciboNomina( String nombre, int horasTrabNormal, int horasTrabExtras, float impuesto,int puesto) {

        Nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.impuesto = impuesto;
        this.puesto = puesto;
    }

    public ReciboNomina() {
    }



    public String getNombre() {
        return Nombre;
    }

    public int getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public int getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public float getImpuesto() {
        return impuesto;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public void setHorasTrabNormal(int horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public void setHorasTrabExtras(int horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public void setImpuesto(float impuesto) {
        this.impuesto = impuesto;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float calcularSubtotal (){

        if (this.puesto == 1){
            subtotal = (50 * this.horasTrabNormal) + (100*this.horasTrabExtras);
        }else if (this.puesto == 2){
            subtotal = (70 * this.horasTrabNormal) + (140*this.horasTrabExtras);
        }else{
            subtotal = (100 * this.horasTrabNormal) + (200*this.horasTrabExtras);
        }
        total = subtotal;

        return subtotal;
    }

    public float calcularImpuesto (){
        return subtotal * 16/100;
    }

    public float calcularTotal (){
        return total-calcularImpuesto();
    }
}
