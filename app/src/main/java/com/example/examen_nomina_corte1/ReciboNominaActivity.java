package com.example.examen_nomina_corte1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    ReciboNomina recibo;
    EditText txtrecibo;
    TextView lblnombre;
    EditText txthorat;
    EditText txthorae;
    RadioButton rdalba;
    RadioButton rdaux;
    RadioButton rding;
    TextView lblsub;
    TextView lbllimpu;
    TextView lbltotal;
    Button btncalcular;
    Button btnlimpiar;
    Button btnregresar;
    RadioGroup rdpuesto;
    EditText txtnumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        lblnombre = (TextView) findViewById(R.id.lblnombre);
        txthorat = (EditText) findViewById(R.id.txthorat);
        txthorae = (EditText) findViewById(R.id.txthorae);
        rdalba = (RadioButton) findViewById(R.id.rdalba);
        rdaux = (RadioButton) findViewById(R.id.rdaux);
        rding = (RadioButton) findViewById(R.id.rding);
        lblsub = (TextView) findViewById(R.id.lblsub);
        lbllimpu = (TextView) findViewById(R.id.lblimpu);
        lbltotal  = (TextView) findViewById(R.id.lbltotal);
        btncalcular = (Button) findViewById(R.id.btncalcular);
        btnlimpiar = (Button) findViewById(R.id.btnlimpiar);
        btnregresar = (Button) findViewById(R.id.btnregresar);
        rdpuesto = (RadioGroup) findViewById(R.id.rdpuesto);
        txtnumero = (EditText) findViewById(R.id.txtnumero);
        Bundle datos = getIntent().getExtras();
        lblnombre.setText("Nonbre: " + (datos.getString("nombre")));


        recibo = (ReciboNomina) datos.getSerializable("recibo");


        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String hora = txthorat.getText().toString();
                String horaextra = txthorae.getText().toString();


                if(hora.matches("") || horaextra.matches("") || txtnumero.getText().toString().matches("" +
                        "" +
                        "" +
                        "")){
                    Toast.makeText(ReciboNominaActivity.this, "Falta campos", Toast.LENGTH_SHORT).show();
                }else{
                    recibo.setHorasTrabNormal(Integer.valueOf(hora));
                    recibo.setHorasTrabExtras(Integer.valueOf(horaextra));

                    if(rdaux.isChecked()){
                        recibo.setPuesto(Integer.valueOf(1));
                    }else if(rdalba.isChecked()){
                        recibo.setPuesto(Integer.valueOf(2));
                    }else {
                        recibo.setPuesto(Integer.valueOf(3));
                    }

                    lblsub.setText("Subtotal: " + String.valueOf(recibo.calcularSubtotal()));
                    lbllimpu.setText("Impuesto: " + String.valueOf(recibo.calcularImpuesto()));
                    lbltotal.setText("Ttotal: " + String.valueOf(recibo.calcularTotal()));
                }

            }
        });

        btnlimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txthorat.setText("");
                txthorae.setText("");
                txtnumero.setText("");
                rdpuesto.check(R.id.rdaux);
            }
        });

        btnregresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnlimpiar.performClick();
                finish();
            }
        });
    }
}